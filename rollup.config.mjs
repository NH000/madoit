/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of MaDoIt.
 *
 * MaDoIt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MaDoIt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MaDoIt.  If not, see <https://www.gnu.org/licenses/>.
 */

import path from 'path';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import json from '@rollup/plugin-json';
import { terser } from "@el3um4s/rollup-plugin-terser";

const INPUT_DIR = './js';
const OUTPUT_DIR = './public/js';

export default [
    {
        input: path.join(INPUT_DIR, 'aux.mjs'),
        output: {
            file: path.join(OUTPUT_DIR, 'aux.min.mjs'),
            format: 'es'
        },
        plugins: [nodeResolve(), commonjs(), json({ compact: true }), terser()]
    },
    {
        input: path.join(INPUT_DIR, 'burger.mjs'),
        output: {
            file: path.join(OUTPUT_DIR, 'burger.min.mjs'),
            format: 'es'
        },
        plugins: [terser()]
    },
    {
        input: path.join(INPUT_DIR, 'codemirror-l10n.mjs'),
        output: {
            file: path.join(OUTPUT_DIR, 'codemirror-l10n.min.mjs'),
            format: 'es'
        },
        plugins: [nodeResolve(), terser()]
    },
    {
        input: path.join(INPUT_DIR, 'cookies.mjs'),
        output: {
            file: path.join(OUTPUT_DIR, 'cookies.min.mjs'),
            format: 'es'
        },
        plugins: [nodeResolve(), terser()]
    },
    {
        external: ['/js/aux.min.mjs', '/js/codemirror-l10n.min.mjs'],
        input: path.join(INPUT_DIR, 'edit.mjs'),
        output: {
            file: path.join(OUTPUT_DIR, 'edit.min.mjs'),
            format: 'es'
        },
        plugins: [nodeResolve(), commonjs(), terser()]
    },
    {
        external: ['/js/aux.min.mjs'],
        input: path.join(INPUT_DIR, 'index.mjs'),
        output: {
            file: path.join(OUTPUT_DIR, 'index.min.mjs'),
            format: 'es'
        },
        plugins: [nodeResolve(), terser()]
    },
    {
        input: path.join(INPUT_DIR, 'modals-close.mjs'),
        output: {
            file: path.join(OUTPUT_DIR, 'modals-close.min.mjs'),
            format: 'es'
        },
        plugins: [nodeResolve(), commonjs(), terser()]
    },
    {
        input: path.join(INPUT_DIR, 'notifications-close.mjs'),
        output: {
            file: path.join(OUTPUT_DIR, 'notifications-close.min.mjs'),
            format: 'es'
        },
        plugins: [terser()]
    },
    {
        external: ['/js/aux.min.mjs'],
        input: path.join(INPUT_DIR, 'settings.mjs'),
        output: {
            file: path.join(OUTPUT_DIR, 'settings.min.mjs'),
            format: 'es'
        },
        plugins: [nodeResolve({ modulePaths: INPUT_DIR }), terser()]
    },
    {
        input: path.join(INPUT_DIR, 'theme.mjs'),
        output: {
            file: path.join(OUTPUT_DIR, 'theme.min.mjs'),
            format: 'es'
        },
        plugins: [nodeResolve(), terser()]
    },
];
