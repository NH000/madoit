# MaDoIt

## Description
MaDoIt is a simple and easy to use Markdown web editor.
It supports various parser and editor options, as well as a light and a dark theme.
It is suitable to be used on all screen sizes.
Please see the introduction file for more info: `/public/intros/Introduction.md`.

### Todo
+ Add ++inserted++ and ==marked== text syntax highlighting support to the editor ([CodeMirror](https://codemirror.net/))

## Requirements
+ npm
+ node

## Installation
Simply install all dependencies by running the following command in the project root:

```
npm update
```

## Running
In order to run the server you first must navigate to the project root and add your key and certificate under `httpsOptions` in `server.mjs`.
Then run the server like this:

```
node server.mjs --cookies --cookies-secure
```

You ***must*** run the server with cookies enabled in order for everything to work properly.
You can get more server options by running `node server.mjs --help`.

*NOTE: If you are interested in the server file template, check out [this snippet](https://gitlab.com/-/snippets/2415365).*

### On localhost
If you just want to run the server on the localhost, you don't need key and certificate and this will do (but no HTTPS!):

```
node server.mjs --http-port 8000 --no-https --cookies --cookies-secure
```

## Translations
Default language of this web app is English (US).
That's the language in which the website will be displayed to users in a country for whose language a translation is not available.

The translations available are:

| **Language**       | **Translator**                                           | **For versions** |
|--------------------|----------------------------------------------------------|------------------|
| Serbian (Cyrillic) | [Nikola Hadžić](mailto:nikola.hadzic.000@protonmail.com) | 1.0.0            |
| Serbian (Latin)    | [Nikola Hadžić](mailto:nikola.hadzic.000@protonmail.com) | 1.0.0            |

### Adding a translation
If you want to add a translation, enter the project root and follow this process:

1. Open the `server.mjs` file and add the corresponding locale to the `locales` field of the object passed in the call to `i18n.configure()`.
1. Translate the introduction file (`public/intros/Introduction.md`) and place the translation in the same directory.
1. Register the new introduction file with server by adding it to the `intro` object in `server.mjs`.
1. Optionally add locale fallbacks to `localeFallbacks` object in `server.mjs`.
1. Add the new locale to the settings page (`views/settings.pug`) `#settings-global-language` select tag.
1. Run the server and visit every website page with the website language set to the one you are doing the translation for (which you can do through the website
settings page). This will create and populate the `l10n/` translation file for your locale.
1. Open the `l10n/` file for your locale and translate the strings in it. After this, you will have to restart the server to see the changes on the website.
1. Translate the editor in the same way as the previous step by editing `js/codemirror-l10n.mjs` file, and then regenerate it with `npm run js-build`.
