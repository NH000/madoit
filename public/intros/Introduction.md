# MaDoIt
## Web Markdown editor

---

### Features
+ Looks and works well on all screen sizes
+ Support for CommonMark and GFM flavors, as well as custom parser options
    + Newline breaks
    + Autolinks
    + Tables
    + Strikethrough
    + Header anchors
    + Subscripts
    + Superscripts
    + Footnotes
    + Definition lists
    + Task lists
    + Abbreviations
    + Emojis
    + Insert
    + Mark
+ Editor options
    + Syntax highlighting
    + Line numbers
    + Code folding
    + Bracket matching
    + Bracket closing
    + Rectangular selection
    + Active line highlight
    + Selection highlight
    + Search / Replace
+ Preview while editing
+ Light and dark theme

### About
Made by [Nikola Hadžić](https://gitlab.com/NH000) and hosted [here](https://gitlab.com/NH000/madoit).

The backend uses [Node.js](https://nodejs.org/) and [Express](https://expressjs.com/) for serving webpages, and [Pug](https://pugjs.org/) for generating them.  
The frontend code is generated using [rollup.js](https://www.rollupjs.org/).  
[Ubuntu](https://fonts.google.com/specimen/Ubuntu) is the font used for webpages generally,
and [Ubuntu-Mono](https://fonts.google.com/specimen/Ubuntu+Mono) is the font used for code.
[Bulma](https://bulma.io/) is responsible for styling.  
[CodeMirror](https://codemirror.net/) is the editor.

Licensed under [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html).
