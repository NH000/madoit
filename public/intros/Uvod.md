# MaDoIt                                                                           
## Veb Markdown uređivač                                                           
                                                                                   
---                                                                                
                                                                                   
### Odlike
+ Izgleda i radi dobro na svim veličinama ekrana
+ Podrška za CommonMark i GFM ukuse, kao i za parser opcije po izboru
    + Prelomi na novim linijima                                                    
    + Autolinkovi                                                                  
    + Tabele                                                                       
    + Precrtani tekst                                                              
    + Sidra za zaglavlja                                                            
    + Potpisnice                                                                   
    + Natpisnice
    + Fusnote
    + Definicione liste                                                            
    + Zadatne liste                                                                
    + Skraćenice
    + Emotikoni                                                                    
    + Ubačeni tekst                                                                
    + Obeleženi tekst                                                              
+ Opcije uređivača                                                                 
    + Isticanje sintakse
    + Brojevi linija                                                               
    + Presavijanje koda                                                             
    + Podudaranje zagrada                                                           
    + Zatvaranje zagrada                                                            
    + Pravougaona selekcija                                                        
    + Isticanje aktivne linije                                                      
    + Isticanje selekcije
    + Pretraži / Zameni                                                            
+ Prikaz za vreme uređivanja                                                        
+ Svetla i tamna tema
                                                                                   
### O veb aplikaciji                                                               
Napravio [Nikola Hadžić](https://gitlab.com/NH000). Nalazi se [ovde](https://gitlab.com/NH000/madoit).
                                                                                   
Bekend koristi [Node.js](https://nodejs.org/) i [Express](https://expressjs.com/) za služenje veb stranica, i [Pug](https://pugjs.org/) za njihovu generaciju.  
Frontend kod se generiše uz pomoć [rollup.js](https://www.rollupjs.org/) aplikacije.  
[Ubuntu](https://fonts.google.com/specimen/Ubuntu) je font koji se koristi za veb stranice generalno,
a [Ubuntu-Mono](https://fonts.google.com/specimen/Ubuntu+Mono) je font koji se koristi za kod.
[Bulma](https://bulma.io/) je odgovorna za stilizaciju.  
[CodeMirror](https://codemirror.net/) je uređivač.
                                                                                   
Licencirano pod [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html) licencom.
