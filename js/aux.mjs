/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of MaDoIt.
 *
 * MaDoIt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MaDoIt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MaDoIt.  If not, see <https://www.gnu.org/licenses/>.
 */

import Cookies from 'js-cookie';
import MarkdownItHeaderAnchor from 'markdown-it-anchor';
import MarkdownItSub from 'markdown-it-sub';
import MarkdownItSuper from 'markdown-it-sup';
import MarkdownItFootnote from 'markdown-it-footnote';
import MarkdownItDeflist from 'markdown-it-deflist';
import MarkdownItTasklist from 'markdown-it-task-lists';
import MarkdownItAbbr from 'markdown-it-abbr';
import MarkdownItEmoji from 'markdown-it-emoji';
import MarkdownItIns from 'markdown-it-ins';
import MarkdownItMark from 'markdown-it-mark';

/*
 * Dependencies: js-cookie.
 * Testing option state: variant #1.
 * If the session storage option is set to `true`, or if it is null and (global) cookie option is `true` or undefined,
 * true is returned; otherwise false. It is assumed that if the option value is set it is either `true` or `false`.
 */
export function isOptionOn1(session, global) {
    if (session != null) {
        const sessionOption = sessionStorage.getItem(session);
        if (sessionOption === 'true') {
            return true;
        } else if (sessionOption === 'false') {
            return false;
        }
    }

    const globalOption = Cookies.get(global);
    return globalOption !== 'false';
}

// Returns the MarkdownIt parser, setup according to the current settings.
export function getMarkdownParser() {
    let mdFlavor = sessionStorage.getItem(StorageSessionParserFlavor);
    mdFlavor = mdFlavor === null ? MarkdownFlavorDefault : mdFlavor;

    let MarkdownIt = markdownit('default', {
        breaks: mdFlavor === 'custom' && sessionStorage.getItem(StorageSessionParserBreak) === 'true',
        linkify: mdFlavor === 'gfm' || (mdFlavor === 'custom' && sessionStorage.getItem(StorageSessionParserAutolink) === 'true'),
    });

    switch (mdFlavor) {
        case 'custom':
            if (sessionStorage.getItem(StorageSessionParserTable) !== 'true') {
                MarkdownIt = MarkdownIt.disable('table');
            }
            if (sessionStorage.getItem(StorageSessionParserStrikethrough) !== 'true') {
                MarkdownIt = MarkdownIt.disable('strikethrough');
            }
            if (sessionStorage.getItem(StorageSessionParserHeaderAnchor) === 'true') {
                MarkdownIt = MarkdownIt.use(MarkdownItHeaderAnchor);
            }
            if (sessionStorage.getItem(StorageSessionParserSub) === 'true') {
                MarkdownIt = MarkdownIt.use(MarkdownItSub);
            }
            if (sessionStorage.getItem(StorageSessionParserSuper) === 'true') {
                MarkdownIt = MarkdownIt.use(MarkdownItSuper);
            }
            if (sessionStorage.getItem(StorageSessionParserFootnote) === 'true') {
                MarkdownIt = MarkdownIt.use(MarkdownItFootnote);
            }
            if (sessionStorage.getItem(StorageSessionParserDeflist) === 'true') {
                MarkdownIt = MarkdownIt.use(MarkdownItDeflist);
            }
            if (sessionStorage.getItem(StorageSessionParserTasklist) === 'true') {
                MarkdownIt = MarkdownIt.use(MarkdownItTasklist);
            }
            if (sessionStorage.getItem(StorageSessionParserAbbr) === 'true') {
                MarkdownIt = MarkdownIt.use(MarkdownItAbbr);
            }
            if (sessionStorage.getItem(StorageSessionParserEmoji) === 'true') {
                MarkdownIt = MarkdownIt.use(MarkdownItEmoji);
            }
            if (sessionStorage.getItem(StorageSessionParserIns) === 'true') {
                MarkdownIt = MarkdownIt.use(MarkdownItIns);
            }
            if (sessionStorage.getItem(StorageSessionParserMark) === 'true') {
                MarkdownIt = MarkdownIt.use(MarkdownItMark);
            }
            break;
        case 'commonmark':
            MarkdownIt = MarkdownIt.disable(['table', 'strikethrough']);
            break;
        case 'gfm':
            MarkdownIt = MarkdownIt.use(MarkdownItHeaderAnchor).use(MarkdownItSub).use(MarkdownItSuper).use(MarkdownItFootnote).use(MarkdownItTasklist).use(MarkdownItEmoji);
            break;
    }

    return MarkdownIt;
}

// Sets up the Markdown previewer.
export function updateMarkdownPreview(mdParser, text) {
    $('.md-preview').html(mdParser.render(text));
    $('.md-preview a:is([href^="http://"], [href^="https://"])').attr('target', '_blank');
    $('.md-preview a:not([href^="#"], [href^="http://"], [href^="https://"])').click((ev) => ev.preventDefault());
}
