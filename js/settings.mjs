/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of MaDoIt.
 *
 * MaDoIt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MaDoIt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MaDoIt.  If not, see <https://www.gnu.org/licenses/>.
 */

import Cookies from 'js-cookie';

import { isOptionOn1 } from '/js/aux.min.mjs';

function onHashChange() {
    switch ($(location).attr('hash')) {
        case '#session':
            $('#settings-link-global').parent().removeClass('is-active');
            $('#settings-link-session').parent().addClass('is-active');
            $('#settings-global').addClass('is-hidden');
            $('#settings-session').removeClass('is-hidden');
            break;
        case '#global':
            $('#settings-link-session').parent().removeClass('is-active');
            $('#settings-link-global').parent().addClass('is-active');
            $('#settings-session').addClass('is-hidden');
            $('#settings-global').removeClass('is-hidden');
            break;
        default:
            break;
    }
}

function onParserFlavorChange(settingsSection, options) {
    const selectFlavor = $('#settings-' + settingsSection + '-parser-flavor');
    const checkboxBreak = $('#settings-' + settingsSection + '-parser-break');
    const checkboxAutolink = $('#settings-' + settingsSection + '-parser-autolink');
    const checkboxTable = $('#settings-' + settingsSection + '-parser-table');
    const checkboxHeaderAnchor = $('#settings-' + settingsSection + '-parser-header-anchor');
    const checkboxStrikethrough = $('#settings-' + settingsSection + '-parser-strikethrough');
    const checkboxSub = $('#settings-' + settingsSection + '-parser-sub');
    const checkboxSuper = $('#settings-' + settingsSection + '-parser-super');
    const checkboxFootnote = $('#settings-' + settingsSection + '-parser-footnote');
    const checkboxDeflist = $('#settings-' + settingsSection + '-parser-deflist');
    const checkboxTasklist = $('#settings-' + settingsSection + '-parser-tasklist');
    const checkboxAbbr = $('#settings-' + settingsSection + '-parser-abbr');
    const checkboxEmoji = $('#settings-' + settingsSection + '-parser-emoji');
    const checkboxIns = $('#settings-' + settingsSection + '-parser-ins');
    const checkboxMark = $('#settings-' + settingsSection + '-parser-mark');

    if (options !== undefined) {
        selectFlavor.val(options.flavor);
    }

    const flavor = options === undefined ? selectFlavor.val() : options.flavor;

    switch (flavor) {
        case 'custom':
            if (options !== undefined) {
                checkboxBreak.prop('checked', options.break);
                checkboxAutolink.prop('checked', options.autolink);
                checkboxTable.prop('checked', options.table);
                checkboxStrikethrough.prop('checked', options.strikethrough);
                checkboxHeaderAnchor.prop('checked', options.headerAnchor);
                checkboxSub.prop('checked', options.sub);
                checkboxSuper.prop('checked', options.super);
                checkboxFootnote.prop('checked', options.footnote);
                checkboxDeflist.prop('checked', options.deflist);
                checkboxTasklist.prop('checked', options.tasklist);
                checkboxAbbr.prop('checked', options.abbr);
                checkboxEmoji.prop('checked', options.emoji);
                checkboxIns.prop('checked', options.ins);
                checkboxMark.prop('checked', options.mark);
            }
            break;
        case 'commonmark':
            checkboxBreak.prop('checked', false);
            checkboxAutolink.prop('checked', false);
            checkboxTable.prop('checked', false);
            checkboxStrikethrough.prop('checked', false);
            checkboxHeaderAnchor.prop('checked', false);
            checkboxSub.prop('checked', false);
            checkboxSuper.prop('checked', false);
            checkboxFootnote.prop('checked', false);
            checkboxDeflist.prop('checked', false);
            checkboxTasklist.prop('checked', false);
            checkboxAbbr.prop('checked', false);
            checkboxEmoji.prop('checked', false);
            checkboxIns.prop('checked', false);
            checkboxMark.prop('checked', false);
            checkboxMark.prop('checked', false);
            break;
        case 'gfm':
            checkboxBreak.prop('checked', false);
            checkboxAutolink.prop('checked', true);
            checkboxTable.prop('checked', true);
            checkboxStrikethrough.prop('checked', true);
            checkboxHeaderAnchor.prop('checked', true);
            checkboxSub.prop('checked', true);
            checkboxSuper.prop('checked', true);
            checkboxFootnote.prop('checked', true);
            checkboxDeflist.prop('checked', false);
            checkboxTasklist.prop('checked', true);
            checkboxAbbr.prop('checked', false);
            checkboxEmoji.prop('checked', true);
            checkboxIns.prop('checked', false);
            checkboxMark.prop('checked', false);
            break;
    }

    const isFlavorNotCustom = flavor !== 'custom';
    checkboxBreak.prop('disabled', isFlavorNotCustom);
    checkboxAutolink.prop('disabled', isFlavorNotCustom);
    checkboxTable.prop('disabled', isFlavorNotCustom);
    checkboxStrikethrough.prop('disabled', isFlavorNotCustom);
    checkboxHeaderAnchor.prop('disabled', isFlavorNotCustom);
    checkboxSub.prop('disabled', isFlavorNotCustom);
    checkboxSuper.prop('disabled', isFlavorNotCustom);
    checkboxFootnote.prop('disabled', isFlavorNotCustom);
    checkboxDeflist.prop('disabled', isFlavorNotCustom);
    checkboxTasklist.prop('disabled', isFlavorNotCustom);
    checkboxAbbr.prop('disabled', isFlavorNotCustom);
    checkboxEmoji.prop('disabled', isFlavorNotCustom);
    checkboxIns.prop('disabled', isFlavorNotCustom);
    checkboxMark.prop('disabled', isFlavorNotCustom);
}

function saveParserFlavor(settingsSection) {
    const selectFlavor = $('#settings-' + settingsSection + '-parser-flavor');
    const checkboxBreak = $('#settings-' + settingsSection + '-parser-break');
    const checkboxAutolink = $('#settings-' + settingsSection + '-parser-autolink');
    const checkboxTable = $('#settings-' + settingsSection + '-parser-table');
    const checkboxStrikethrough = $('#settings-' + settingsSection + '-parser-strikethrough');
    const checkboxHeaderAnchor = $('#settings-' + settingsSection + '-parser-header-anchor');
    const checkboxSub = $('#settings-' + settingsSection + '-parser-sub');
    const checkboxSuper = $('#settings-' + settingsSection + '-parser-super');
    const checkboxFootnote = $('#settings-' + settingsSection + '-parser-footnote');
    const checkboxDeflist = $('#settings-' + settingsSection + '-parser-deflist');
    const checkboxTasklist = $('#settings-' + settingsSection + '-parser-tasklist');
    const checkboxAbbr = $('#settings-' + settingsSection + '-parser-abbr');
    const checkboxEmoji = $('#settings-' + settingsSection + '-parser-emoji');
    const checkboxIns = $('#settings-' + settingsSection + '-parser-ins');
    const checkboxMark = $('#settings-' + settingsSection + '-parser-mark');

    const flavor = selectFlavor.val();

    switch (settingsSection) {
        case 'session':
            if (flavor === 'custom') {
                sessionStorage.setItem(StorageSessionParserBreak, checkboxBreak.is(':checked'));
                sessionStorage.setItem(StorageSessionParserAutolink, checkboxAutolink.is(':checked'));
                sessionStorage.setItem(StorageSessionParserTable, checkboxTable.is(':checked'));
                sessionStorage.setItem(StorageSessionParserStrikethrough, checkboxStrikethrough.is(':checked'));
                sessionStorage.setItem(StorageSessionParserHeaderAnchor, checkboxHeaderAnchor.is(':checked'));
                sessionStorage.setItem(StorageSessionParserSub, checkboxSub.is(':checked'));
                sessionStorage.setItem(StorageSessionParserSuper, checkboxSuper.is(':checked'));
                sessionStorage.setItem(StorageSessionParserFootnote, checkboxFootnote.is(':checked'));
                sessionStorage.setItem(StorageSessionParserDeflist, checkboxDeflist.is(':checked'));
                sessionStorage.setItem(StorageSessionParserTasklist, checkboxTasklist.is(':checked'));
                sessionStorage.setItem(StorageSessionParserAbbr, checkboxAbbr.is(':checked'));
                sessionStorage.setItem(StorageSessionParserEmoji, checkboxEmoji.is(':checked'));
                sessionStorage.setItem(StorageSessionParserIns, checkboxIns.is(':checked'));
                sessionStorage.setItem(StorageSessionParserMark, checkboxMark.is(':checked'));
            } else {
                sessionStorage.removeItem(StorageSessionParserBreak);
                sessionStorage.removeItem(StorageSessionParserAutolink);
                sessionStorage.removeItem(StorageSessionParserTable);
                sessionStorage.removeItem(StorageSessionParserStrikethrough);
                sessionStorage.removeItem(StorageSessionParserHeaderAnchor);
                sessionStorage.removeItem(StorageSessionParserSub);
                sessionStorage.removeItem(StorageSessionParserSuper);
                sessionStorage.removeItem(StorageSessionParserFootnote);
                sessionStorage.removeItem(StorageSessionParserDeflist);
                sessionStorage.removeItem(StorageSessionParserTasklist);
                sessionStorage.removeItem(StorageSessionParserAbbr);
                sessionStorage.removeItem(StorageSessionParserEmoji);
                sessionStorage.removeItem(StorageSessionParserIns);
                sessionStorage.removeItem(StorageSessionParserMark);
            }

            sessionStorage.setItem(StorageSessionParserFlavor, flavor);
            break;
        case 'global':
            if (flavor === 'custom') {
                Cookies.set(CookieGlobalParserBreak, checkboxBreak.is(':checked'), {
                    sameSite:   CookiesPolicy,
                    secure:     cookiesSecure,
                    expires:    cookiesExpire
                });
                Cookies.set(CookieGlobalParserAutolink, checkboxAutolink.is(':checked'), {
                    sameSite:   CookiesPolicy,
                    secure:     cookiesSecure,
                    expires:    cookiesExpire
                });
                Cookies.set(CookieGlobalParserTable, checkboxTable.is(':checked'), {
                    sameSite:   CookiesPolicy,
                    secure:     cookiesSecure,
                    expires:    cookiesExpire
                });
                Cookies.set(CookieGlobalParserStrikethrough, checkboxStrikethrough.is(':checked'), {
                    sameSite:   CookiesPolicy,
                    secure:     cookiesSecure,
                    expires:    cookiesExpire
                });
                Cookies.set(CookieGlobalParserHeaderAnchor, checkboxHeaderAnchor.is(':checked'), {
                    sameSite:   CookiesPolicy,
                    secure:     cookiesSecure,
                    expires:    cookiesExpire
                });
                Cookies.set(CookieGlobalParserSub, checkboxSub.is(':checked'), {
                    sameSite:   CookiesPolicy,
                    secure:     cookiesSecure,
                    expires:    cookiesExpire
                });
                Cookies.set(CookieGlobalParserSuper, checkboxSuper.is(':checked'), {
                    sameSite:   CookiesPolicy,
                    secure:     cookiesSecure,
                    expires:    cookiesExpire
                });
                Cookies.set(CookieGlobalParserFootnote, checkboxFootnote.is(':checked'), {
                    sameSite:   CookiesPolicy,
                    secure:     cookiesSecure,
                    expires:    cookiesExpire
                });
                Cookies.set(CookieGlobalParserDeflist, checkboxDeflist.is(':checked'), {
                    sameSite:   CookiesPolicy,
                    secure:     cookiesSecure,
                    expires:    cookiesExpire
                });
                Cookies.set(CookieGlobalParserTasklist, checkboxTasklist.is(':checked'), {
                    sameSite:   CookiesPolicy,
                    secure:     cookiesSecure,
                    expires:    cookiesExpire
                });
                Cookies.set(CookieGlobalParserAbbr, checkboxAbbr.is(':checked'), {
                    sameSite:   CookiesPolicy,
                    secure:     cookiesSecure,
                    expires:    cookiesExpire
                });
                Cookies.set(CookieGlobalParserEmoji, checkboxEmoji.is(':checked'), {
                    sameSite:   CookiesPolicy,
                    secure:     cookiesSecure,
                    expires:    cookiesExpire
                });
                Cookies.set(CookieGlobalParserIns, checkboxIns.is(':checked'), {
                    sameSite:   CookiesPolicy,
                    secure:     cookiesSecure,
                    expires:    cookiesExpire
                });
                Cookies.set(CookieGlobalParserMark, checkboxMark.is(':checked'), {
                    sameSite:   CookiesPolicy,
                    secure:     cookiesSecure,
                    expires:    cookiesExpire
                });
            } else {
                Cookies.remove(CookieGlobalParserBreak);
                Cookies.remove(CookieGlobalParserAutolink);
                Cookies.remove(CookieGlobalParserTable);
                Cookies.remove(CookieGlobalParserStrikethrough);
                Cookies.remove(CookieGlobalParserHeaderAnchor);
                Cookies.remove(CookieGlobalParserSub);
                Cookies.remove(CookieGlobalParserSuper);
                Cookies.remove(CookieGlobalParserFootnote);
                Cookies.remove(CookieGlobalParserDeflist);
                Cookies.remove(CookieGlobalParserTasklist);
                Cookies.remove(CookieGlobalParserAbbr);
                Cookies.remove(CookieGlobalParserEmoji);
                Cookies.remove(CookieGlobalParserIns);
                Cookies.remove(CookieGlobalParserMark);
            }

            Cookies.set(CookieGlobalParserFlavor, flavor, {
                sameSite:   CookiesPolicy,
                secure:     cookiesSecure,
                expires:    cookiesExpire
            });
            break;
    }
}

/*
 * function setEditor(settingsSection, options) {
 * }
 * 
 * function saveEditor(settingsSection) {
 *     switch (settingsSection) {
 *         case 'session':
 *             break;
 *         case 'global':
 *             break;
 *     }
 * }
 */

function onEditorExtensionsSyntaxHighlightChange(settingsSection, newValue) {
    const checkboxSyntaxHighlightTable = $('#settings-' + settingsSection + '-editor-syntax-highlight-table');
    const checkboxSyntaxHighlightStrikethrough = $('#settings-' + settingsSection + '-editor-syntax-highlight-strikethrough');
    const checkboxSyntaxHighlightSub = $('#settings-' + settingsSection + '-editor-syntax-highlight-sub');
    const checkboxSyntaxHighlightSuper = $('#settings-' + settingsSection + '-editor-syntax-highlight-super');
    const checkboxSyntaxHighlightTasklist = $('#settings-' + settingsSection + '-editor-syntax-highlight-tasklist');
    const checkboxSyntaxHighlightEmoji = $('#settings-' + settingsSection + '-editor-syntax-highlight-emoji');

    checkboxSyntaxHighlightTable.prop({ checked: newValue, disabled: !newValue });
    checkboxSyntaxHighlightStrikethrough.prop({ checked: newValue, disabled: !newValue });
    checkboxSyntaxHighlightSub.prop({ checked: newValue, disabled: !newValue });
    checkboxSyntaxHighlightSuper.prop({ checked: newValue, disabled: !newValue });
    checkboxSyntaxHighlightTasklist.prop({ checked: newValue, disabled: !newValue });
    checkboxSyntaxHighlightEmoji.prop({ checked: newValue, disabled: !newValue });
}

function setEditorExtensions(settingsSection, options) {
    const checkboxSyntaxHighlight = $('#settings-' + settingsSection + '-editor-syntax-highlight');
    const checkboxSyntaxHighlightTable = $('#settings-' + settingsSection + '-editor-syntax-highlight-table');
    const checkboxSyntaxHighlightStrikethrough = $('#settings-' + settingsSection + '-editor-syntax-highlight-strikethrough');
    const checkboxSyntaxHighlightSub = $('#settings-' + settingsSection + '-editor-syntax-highlight-sub');
    const checkboxSyntaxHighlightSuper = $('#settings-' + settingsSection + '-editor-syntax-highlight-super');
    const checkboxSyntaxHighlightTasklist = $('#settings-' + settingsSection + '-editor-syntax-highlight-tasklist');
    const checkboxSyntaxHighlightEmoji = $('#settings-' + settingsSection + '-editor-syntax-highlight-emoji');
    const checkboxLineNumbers = $('#settings-' + settingsSection + '-editor-line-numbers');
    const checkboxFolding = $('#settings-' + settingsSection + '-editor-folding');
    const checkboxBracketMatching = $('#settings-' + settingsSection + '-editor-bracket-matching');
    const checkboxBracketClosing = $('#settings-' + settingsSection + '-editor-bracket-closing');
    const checkboxRectSelection = $('#settings-' + settingsSection + '-editor-rect-selection');
    const checkboxActiveLineHighlight = $('#settings-' + settingsSection + '-editor-active-line-highlight');
    const checkboxSelectionHighlight = $('#settings-' + settingsSection + '-editor-selection-highlight');
    const checkboxSearch = $('#settings-' + settingsSection + '-editor-search');

    checkboxSyntaxHighlight.prop('checked', options.syntaxHighlight);
    checkboxSyntaxHighlightTable.prop('checked', options.syntaxHighlight && options.syntaxHighlightTable);
    checkboxSyntaxHighlightStrikethrough.prop('checked', options.syntaxHighlight && options.syntaxHighlightStrikethrough);
    checkboxSyntaxHighlightSub.prop('checked', options.syntaxHighlight && options.syntaxHighlightSub);
    checkboxSyntaxHighlightSuper.prop('checked', options.syntaxHighlight && options.syntaxHighlightSuper);
    checkboxSyntaxHighlightTasklist.prop('checked', options.syntaxHighlight && options.syntaxHighlightTasklist);
    checkboxSyntaxHighlightEmoji.prop('checked', options.syntaxHighlight && options.syntaxHighlightEmoji);
    checkboxLineNumbers.prop('checked', options.lineNumbers);
    checkboxFolding.prop('checked', options.folding);
    checkboxBracketMatching.prop('checked', options.bracketMatching);
    checkboxBracketClosing.prop('checked', options.bracketClosing);
    checkboxRectSelection.prop('checked', options.rectSelection);
    checkboxActiveLineHighlight.prop('checked', options.activeLineHighlight);
    checkboxSelectionHighlight.prop('checked', options.selectionHighlight);
    checkboxSearch.prop('checked', options.search);

    checkboxSyntaxHighlightTable.prop('disabled', !options.syntaxHighlight);
    checkboxSyntaxHighlightStrikethrough.prop('disabled', !options.syntaxHighlight);
    checkboxSyntaxHighlightSub.prop('disabled', !options.syntaxHighlight);
    checkboxSyntaxHighlightSuper.prop('disabled', !options.syntaxHighlight);
    checkboxSyntaxHighlightTasklist.prop('disabled', !options.syntaxHighlight);
    checkboxSyntaxHighlightEmoji.prop('disabled', !options.syntaxHighlight);
}

function saveEditorExtensions(settingsSection) {
    const checkboxSyntaxHighlight = $('#settings-' + settingsSection + '-editor-syntax-highlight');
    const checkboxSyntaxHighlightTable = $('#settings-' + settingsSection + '-editor-syntax-highlight-table');
    const checkboxSyntaxHighlightStrikethrough = $('#settings-' + settingsSection + '-editor-syntax-highlight-strikethrough');
    const checkboxSyntaxHighlightSub = $('#settings-' + settingsSection + '-editor-syntax-highlight-sub');
    const checkboxSyntaxHighlightSuper = $('#settings-' + settingsSection + '-editor-syntax-highlight-super');
    const checkboxSyntaxHighlightTasklist = $('#settings-' + settingsSection + '-editor-syntax-highlight-tasklist');
    const checkboxSyntaxHighlightEmoji = $('#settings-' + settingsSection + '-editor-syntax-highlight-emoji');
    const checkboxLineNumbers = $('#settings-' + settingsSection + '-editor-line-numbers');
    const checkboxFolding = $('#settings-' + settingsSection + '-editor-folding');
    const checkboxBracketMatching = $('#settings-' + settingsSection + '-editor-bracket-matching');
    const checkboxBracketClosing = $('#settings-' + settingsSection + '-editor-bracket-closing');
    const checkboxRectSelection = $('#settings-' + settingsSection + '-editor-rect-selection');
    const checkboxActiveLineHighlight = $('#settings-' + settingsSection + '-editor-active-line-highlight');
    const checkboxSelectionHighlight = $('#settings-' + settingsSection + '-editor-selection-highlight');
    const checkboxSearch = $('#settings-' + settingsSection + '-editor-search');

    switch (settingsSection) {
        case 'session':
            sessionStorage.setItem(StorageSessionEditorSyntaxHighlight, checkboxSyntaxHighlight.is(':checked'));
            sessionStorage.setItem(StorageSessionEditorSyntaxHighlightTable, checkboxSyntaxHighlightTable.is(':checked'));
            sessionStorage.setItem(StorageSessionEditorSyntaxHighlightStrikethrough, checkboxSyntaxHighlightStrikethrough.is(':checked'));
            sessionStorage.setItem(StorageSessionEditorSyntaxHighlightSub, checkboxSyntaxHighlightSub.is(':checked'));
            sessionStorage.setItem(StorageSessionEditorSyntaxHighlightSuper, checkboxSyntaxHighlightSuper.is(':checked'));
            sessionStorage.setItem(StorageSessionEditorSyntaxHighlightTasklist, checkboxSyntaxHighlightTasklist.is(':checked'));
            sessionStorage.setItem(StorageSessionEditorSyntaxHighlightEmoji, checkboxSyntaxHighlightEmoji.is(':checked'));
            sessionStorage.setItem(StorageSessionEditorLineNumbers, checkboxLineNumbers.is(':checked'));
            sessionStorage.setItem(StorageSessionEditorFolding, checkboxFolding.is(':checked'));
            sessionStorage.setItem(StorageSessionEditorBracketMatching, checkboxBracketMatching.is(':checked'));
            sessionStorage.setItem(StorageSessionEditorBracketClosing, checkboxBracketClosing.is(':checked'));
            sessionStorage.setItem(StorageSessionEditorRectSelection, checkboxRectSelection.is(':checked'));
            sessionStorage.setItem(StorageSessionEditorActiveLineHighlight, checkboxActiveLineHighlight.is(':checked'));
            sessionStorage.setItem(StorageSessionEditorSelectionHighlight, checkboxSelectionHighlight.is(':checked'));
            sessionStorage.setItem(StorageSessionEditorSearch, checkboxSearch.is(':checked'));
            break;
        case 'global':
            Cookies.set(CookieGlobalEditorSyntaxHighlight, checkboxSyntaxHighlight.is(':checked'), {
                sameSite:   CookiesPolicy,
                secure:     cookiesSecure,
                expires:    cookiesExpire
            });
            Cookies.set(CookieGlobalEditorSyntaxHighlightTable, checkboxSyntaxHighlightTable.is(':checked'), {
                sameSite:   CookiesPolicy,
                secure:     cookiesSecure,
                expires:    cookiesExpire
            });
            Cookies.set(CookieGlobalEditorSyntaxHighlightStrikethrough, checkboxSyntaxHighlightStrikethrough.is(':checked'), {
                sameSite:   CookiesPolicy,
                secure:     cookiesSecure,
                expires:    cookiesExpire
            });
            Cookies.set(CookieGlobalEditorSyntaxHighlightSub, checkboxSyntaxHighlightSub.is(':checked'), {
                sameSite:   CookiesPolicy,
                secure:     cookiesSecure,
                expires:    cookiesExpire
            });
            Cookies.set(CookieGlobalEditorSyntaxHighlightSuper, checkboxSyntaxHighlightSuper.is(':checked'), {
                sameSite:   CookiesPolicy,
                secure:     cookiesSecure,
                expires:    cookiesExpire
            });
            Cookies.set(CookieGlobalEditorSyntaxHighlightTasklist, checkboxSyntaxHighlightTasklist.is(':checked'), {
                sameSite:   CookiesPolicy,
                secure:     cookiesSecure,
                expires:    cookiesExpire
            });
            Cookies.set(CookieGlobalEditorSyntaxHighlightEmoji, checkboxSyntaxHighlightEmoji.is(':checked'), {
                sameSite:   CookiesPolicy,
                secure:     cookiesSecure,
                expires:    cookiesExpire
            });
            Cookies.set(CookieGlobalEditorLineNumbers, checkboxLineNumbers.is(':checked'), {
                sameSite:   CookiesPolicy,
                secure:     cookiesSecure,
                expires:    cookiesExpire
            });
            Cookies.set(CookieGlobalEditorFolding, checkboxFolding.is(':checked'), {
                sameSite:   CookiesPolicy,
                secure:     cookiesSecure,
                expires:    cookiesExpire
            });
            Cookies.set(CookieGlobalEditorBracketMatching, checkboxBracketMatching.is(':checked'), {
                sameSite:   CookiesPolicy,
                secure:     cookiesSecure,
                expires:    cookiesExpire
            });
            Cookies.set(CookieGlobalEditorBracketClosing, checkboxBracketClosing.is(':checked'), {
                sameSite:   CookiesPolicy,
                secure:     cookiesSecure,
                expires:    cookiesExpire
            });
            Cookies.set(CookieGlobalEditorRectSelection, checkboxRectSelection.is(':checked'), {
                sameSite:   CookiesPolicy,
                secure:     cookiesSecure,
                expires:    cookiesExpire
            });
            Cookies.set(CookieGlobalEditorActiveLineHighlight, checkboxActiveLineHighlight.is(':checked'), {
                sameSite:   CookiesPolicy,
                secure:     cookiesSecure,
                expires:    cookiesExpire
            });
            Cookies.set(CookieGlobalEditorSelectionHighlight, checkboxSelectionHighlight.is(':checked'), {
                sameSite:   CookiesPolicy,
                secure:     cookiesSecure,
                expires:    cookiesExpire
            });
            Cookies.set(CookieGlobalEditorSearch, checkboxSearch.is(':checked'), {
                sameSite:   CookiesPolicy,
                secure:     cookiesSecure,
                expires:    cookiesExpire
            });
            break;
    }
}

$(document).ready(() => {
    {
        const sessionParserFlavor = sessionStorage.getItem(StorageSessionParserFlavor);
        const globalParserFlavor = Cookies.get(CookieGlobalParserFlavor);

        onParserFlavorChange('session', sessionParserFlavor === null ? { flavor: globalParserFlavor === undefined ? MarkdownFlavorDefault : globalParserFlavor } : {
            flavor: sessionParserFlavor,
            break: sessionStorage.getItem(StorageSessionParserBreak) === 'true',
            autolink: sessionStorage.getItem(StorageSessionParserAutolink) === 'true',
            table: sessionStorage.getItem(StorageSessionParserTable) === 'true',
            strikethrough: sessionStorage.getItem(StorageSessionParserStrikethrough) === 'true',
            headerAnchor: sessionStorage.getItem(StorageSessionParserHeaderAnchor) === 'true',
            sub: sessionStorage.getItem(StorageSessionParserSub) === 'true',
            super: sessionStorage.getItem(StorageSessionParserSuper) === 'true',
            footnote: sessionStorage.getItem(StorageSessionParserFootnote) === 'true',
            deflist: sessionStorage.getItem(StorageSessionParserDeflist) === 'true',
            tasklist: sessionStorage.getItem(StorageSessionParserTasklist) === 'true',
            abbr: sessionStorage.getItem(StorageSessionParserAbbr) === 'true',
            emoji: sessionStorage.getItem(StorageSessionParserEmoji) === 'true',
            ins: sessionStorage.getItem(StorageSessionParserIns) === 'true',
            mark: sessionStorage.getItem(StorageSessionParserMark) === 'true',
        });

        onParserFlavorChange('global', globalParserFlavor === undefined ? { flavor: MarkdownFlavorDefault } : {
            flavor: globalParserFlavor,
            break: Cookies.get(CookieGlobalParserBreak) === 'true',
            autolink: Cookies.get(CookieGlobalParserAutolink) === 'true',
            table: Cookies.get(CookieGlobalParserTable) === 'true',
            strikethrough: Cookies.get(CookieGlobalParserStrikethrough) === 'true',
            headerAnchor: Cookies.get(CookieGlobalParserHeaderAnchor) === 'true',
            sub: Cookies.get(CookieGlobalParserSub) === 'true',
            super: Cookies.get(CookieGlobalParserSuper) === 'true',
            footnote: Cookies.get(CookieGlobalParserFootnote) === 'true',
            deflist: Cookies.get(CookieGlobalParserDeflist) === 'true',
            tasklist: Cookies.get(CookieGlobalParserTasklist) === 'true',
            abbr: Cookies.get(CookieGlobalParserAbbr) === 'true',
            emoji: Cookies.get(CookieGlobalParserEmoji) === 'true',
            ins: Cookies.get(CookieGlobalParserIns) === 'true',
            mark: Cookies.get(CookieGlobalParserMark) === 'true',
        });
    }

    $('#settings-session-parser-flavor').change(() => onParserFlavorChange('session'));
    $('#settings-global-parser-flavor').change(() => onParserFlavorChange('global'));

    $('#settings-session-editor-syntax-highlight').change((ev) => onEditorExtensionsSyntaxHighlightChange('session', $(ev.currentTarget).is(':checked')));
    $('#settings-global-editor-syntax-highlight').change((ev) => onEditorExtensionsSyntaxHighlightChange('global', $(ev.currentTarget).is(':checked')));

    setEditorExtensions('session', {
        syntaxHighlight: isOptionOn1(StorageSessionEditorSyntaxHighlight, CookieGlobalEditorSyntaxHighlight),
        syntaxHighlightTable: isOptionOn1(StorageSessionEditorSyntaxHighlightTable, CookieGlobalEditorSyntaxHighlightTable),
        syntaxHighlightStrikethrough: isOptionOn1(StorageSessionEditorSyntaxHighlightStrikethrough, CookieGlobalEditorSyntaxHighlightStrikethrough),
        syntaxHighlightSub: isOptionOn1(StorageSessionEditorSyntaxHighlightSub, CookieGlobalEditorSyntaxHighlightSub),
        syntaxHighlightSuper: isOptionOn1(StorageSessionEditorSyntaxHighlightSuper, CookieGlobalEditorSyntaxHighlightSuper),
        syntaxHighlightTasklist: isOptionOn1(StorageSessionEditorSyntaxHighlightTasklist, CookieGlobalEditorSyntaxHighlightTasklist),
        syntaxHighlightEmoji: isOptionOn1(StorageSessionEditorSyntaxHighlightEmoji, CookieGlobalEditorSyntaxHighlightEmoji),
        lineNumbers: isOptionOn1(StorageSessionEditorLineNumbers, CookieGlobalEditorLineNumbers),
        folding: isOptionOn1(StorageSessionEditorFolding, CookieGlobalEditorFolding),
        bracketMatching: isOptionOn1(StorageSessionEditorBracketMatching, CookieGlobalEditorBracketMatching),
        bracketClosing: isOptionOn1(StorageSessionEditorBracketClosing, CookieGlobalEditorBracketClosing),
        rectSelection: isOptionOn1(StorageSessionEditorRectSelection, CookieGlobalEditorRectSelection),
        activeLineHighlight: isOptionOn1(StorageSessionEditorActiveLineHighlight, CookieGlobalEditorActiveLineHighlight),
        selectionHighlight: isOptionOn1(StorageSessionEditorSelectionHighlight, CookieGlobalEditorSelectionHighlight),
        search: isOptionOn1(StorageSessionEditorSearch, CookieGlobalEditorSearch),
    });

    setEditorExtensions('global', {
        syntaxHighlight: isOptionOn1(null, CookieGlobalEditorSyntaxHighlight),
        syntaxHighlightTable: isOptionOn1(null, CookieGlobalEditorSyntaxHighlightTable),
        syntaxHighlightStrikethrough: isOptionOn1(null, CookieGlobalEditorSyntaxHighlightStrikethrough),
        syntaxHighlightSub: isOptionOn1(null, CookieGlobalEditorSyntaxHighlightSub),
        syntaxHighlightSuper: isOptionOn1(null, CookieGlobalEditorSyntaxHighlightSuper),
        syntaxHighlightTasklist: isOptionOn1(null, CookieGlobalEditorSyntaxHighlightTasklist),
        syntaxHighlightEmoji: isOptionOn1(null, CookieGlobalEditorSyntaxHighlightEmoji),
        lineNumbers: isOptionOn1(null, CookieGlobalEditorLineNumbers),
        folding: isOptionOn1(null, CookieGlobalEditorFolding),
        bracketMatching: isOptionOn1(null, CookieGlobalEditorBracketMatching),
        bracketClosing: isOptionOn1(null, CookieGlobalEditorBracketClosing),
        rectSelection: isOptionOn1(null, CookieGlobalEditorRectSelection),
        activeLineHighlight: isOptionOn1(null, CookieGlobalEditorActiveLineHighlight),
        selectionHighlight: isOptionOn1(null, CookieGlobalEditorSelectionHighlight),
        search: isOptionOn1(null, CookieGlobalEditorSearch),
    });

    const cookieWarnNotMarkdown = Cookies.get(CookieWarnNotMarkdown);
    $('#settings-global-warn-not-markdown').prop('checked', cookieWarnNotMarkdown === 'true' || cookieWarnNotMarkdown === undefined);

    const cookieTheme = Cookies.get(CookieTheme);
    $('#settings-global-theme').val(cookieTheme === undefined ? 'system' : cookieTheme);

    $('#settings-save').click(() => {
        saveParserFlavor('session');
        saveParserFlavor('global');

        saveEditorExtensions('session');
        saveEditorExtensions('global');

        Cookies.set(CookieWarnNotMarkdown, $('#settings-global-warn-not-markdown').is(':checked'), {
            sameSite:   CookiesPolicy,
            secure:     cookiesSecure,
            expires:    cookiesExpire
        });
        Cookies.set(CookieTheme, $('#settings-global-theme').val(), {
            sameSite:   CookiesPolicy,
            secure:     cookiesSecure,
            expires:    cookiesExpire
        });
        Cookies.set(CookieI18N, $('#settings-global-language').val(), {
            sameSite:   CookiesPolicy,
            secure:     cookiesSecure,
            expires:    cookiesExpire
        });
    });

    onHashChange();

    $(window).on('hashchange', () => {
        onHashChange();
    });
});
