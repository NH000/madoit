/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of MaDoIt.
 *
 * MaDoIt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MaDoIt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MaDoIt.  If not, see <https://www.gnu.org/licenses/>.
 */

import Cookies from 'js-cookie';

$(document).ready(() => {
    if (Cookies.get(CookieCookiesAccepted) === undefined) {
        $('#cookies-modal').addClass('is-active');

        $('#cookies-accept').click(() => {
            $('#cookies-modal').removeClass('is-active');

            Cookies.set(CookieCookiesAccepted, '', {
                sameSite:   'None',
                secure:     CookiesAlwaysSecureForNone || CookiesSecure,
                expires:    cookiesExpire
            });
        });

        $('#cookies-accept').focus();
    }
});
