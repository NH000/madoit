/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the ""Software""), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

export default {
    'sr-Cyrl': {
        // @codemirror/view
        "Control character": "Контролни карактер",
        // @codemirror/commands
        "Selection deleted": "Селекција обрисана",
        // @codemirror/language
        "Folded lines": "Пресавијене линије",
        "Unfolded lines": "Развијене линије",
        "to": "до",
        "folded code": "пресавијен код",
        "unfold": "развиј",
        "Fold line": "Пресавиј линију",
        "Unfold line": "Развиј линију",
        // @codemirror/search
        "Go to line": "Иди на линију",
        "go": "иди",
        "Find": "Пронађи",
        "Replace": "Замени",
        "next": "следеће",
        "previous": "претходно",
        "all": "све",
        "match case": "разликуј велика и мала слова",
        "by word": "по речи",
        "replace": "замени",
        "replace all": "замени све",
        "close": "затвори",
        "current match": "тренутни спој",
        "replaced $ matches": "$ спојева замењено",
        "replaced match on line $": "замењен спој на линији $",
        "on line": "на линији",
        // @codemirror/autocomplete
        "Completions": "Комплетирања",
        // @codemirror/lint
        "Diagnostics": "Дијагностика",
        "No diagnostics": "Нема дијагностике",
        },
    'sr-Latn': {
        // @codemirror/view
        "Control character": "Kontrolni karakter",
        // @codemirror/commands
        "Selection deleted": "Selekcija obrisana",
        // @codemirror/language
        "Folded lines": "Presavijene linije",
        "Unfolded lines": "Razvijene linije",
        "to": "do",
        "folded code": "presavijen kod",
        "unfold": "razvij",
        "Fold line": "Presavij liniju",
        "Unfold line": "Razvij liniju",
        // @codemirror/search
        "Go to line": "Idi na liniju",
        "go": "idi",
        "Find": "Pronađi",
        "Replace": "Zameni",
        "next": "sledeće",
        "previous": "prethodno",
        "all": "sve",
        "match case": "razlikuj velika i mala slova",
        "by word": "po reči",
        "replace": "zameni",
        "replace all": "zameni sve",
        "close": "zatvori",
        "current match": "trenutni spoj",
        "replaced $ matches": "$ spojeva zamenjeno",
        "replaced match on line $": "zamenjen spoj na liniji $",
        "on line": "na liniji",
        // @codemirror/autocomplete
        "Completions": "Kompletiranja",
        // @codemirror/lint
        "Diagnostics": "Dijagnostika",
        "No diagnostics": "Nema dijagnostike",
    }
};
