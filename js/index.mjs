/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of MaDoIt.
 *
 * MaDoIt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MaDoIt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MaDoIt.  If not, see <https://www.gnu.org/licenses/>.
 */

import Cookies from 'js-cookie';

import { getMarkdownParser, updateMarkdownPreview } from '/js/aux.min.mjs';

const mdParser = getMarkdownParser();

function setPreview(title, text) {
    title = title === null ? FileTitleDefault : title;
    text = text === null ? FileContentsDefault : text;

    $('#index-preview-title').text(title);
    updateMarkdownPreview(mdParser, text);

    $('#index-export').prop({
        'href': URL.createObjectURL(new Blob([text], { type: 'text/markdown' })),
        'download': title
    });
}

$(document).ready(() => {
    setPreview(sessionStorage.getItem(StorageFileTitle), sessionStorage.getItem(StorageFileContents));

    $('#index-import').click(() => {
        $('#index-import-input').click();
    });

    $('#index-import-input').change((ev) => {
        const file = ev.target.files[0];

        if (!file.type || file.type.startsWith('text/')) {
            $('#err-not-text').addClass('is-hidden');

            if (!file.type || file.type.endsWith('/markdown')) {
                $('#warn-not-markdown').addClass('is-hidden');
            } else {
                const cookieWarnNotMarkdown = Cookies.get(CookieWarnNotMarkdown);
                if (cookieWarnNotMarkdown === 'true' || cookieWarnNotMarkdown === undefined) {
                    $('#warn-not-markdown').removeClass('is-hidden');
                }
            }

            const fileReader = new FileReader();

            fileReader.addEventListener('error', () => {
                $('#err-load-failure').removeClass('is-hidden');

                $('#index-import').prop('disabled', false);
                $('#index-export').prop('disabled', false);
            });

            fileReader.addEventListener('load', () => {
                $('#err-load-failure').addClass('is-hidden');

                sessionStorage.setItem(StorageFileTitle, file.name);
                sessionStorage.setItem(StorageFileContents, fileReader.result);
                setPreview(file.name, fileReader.result);

                $('#index-import').prop('disabled', false);
                $('#index-export').prop('disabled', false);
            });

            $('#index-import').prop('disabled', true);
            $('#index-export').prop('disabled', true);
            fileReader.readAsText(file);
        } else {
            $('#warn-not-markdown').addClass('is-hidden');
            $('#err-load-failure').addClass('is-hidden');
            $('#err-not-text').removeClass('is-hidden');
        }

        $('.navbar-burger').removeClass('is-active');
        $('.navbar-menu').removeClass('is-active');
    });
});
