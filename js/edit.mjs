/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * This file is part of MaDoIt.
 *
 * MaDoIt is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MaDoIt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MaDoIt.  If not, see <https://www.gnu.org/licenses/>.
 */

import Cookies from 'js-cookie';
import MarkdownItSourceMap from 'markdown-it-source-map';
import * as CodeMirror from 'codemirror';
import * as CodeMirrorState from '@codemirror/state';
import * as CodeMirrorView from '@codemirror/view';
import * as CodeMirrorAutocomplete from '@codemirror/autocomplete';
import * as CodeMirrorSearch from '@codemirror/search';
import * as CodeMirrorLanguage from '@codemirror/language';
import * as CodeMirrorMarkdown from '@codemirror/lang-markdown';
import * as LezerHighlight from '@lezer/highlight';
import * as LezerMarkdown from '@lezer/markdown';

import { isOptionOn1, getMarkdownParser, updateMarkdownPreview } from '/js/aux.min.mjs';
import CodeMirrorL10N from './codemirror-l10n';

const theme = $('#link-theme').attr('title');

const editorTheme = CodeMirrorView.EditorView.theme(theme === 'dark' ? {
    '&': { backgroundColor: '#0A0A0A', color: '#B5B5B5' },
    '&.cm-editor.cm-focused': { outline: 'none' },
    '&.cm-focused .cm-selectionBackground, .cm-content ::selection': { backgroundColor: '#0066CC' },
    '&.cm-focused .cm-matchingBracket, &.cm-focused .cm-nonmatchingBracket': { backgroundColor: '#7500EBFF' },
    '.cm-gutters': { backgroundColor: '#FFBB00', color: '#0A0A0A', },
    '.cm-activeLineGutter': { backgroundColor: '#F5F5F5', color: '#0A0A0A', border: 'none' },
    '.cm-content': { caretColor: '#FFFFFF' },
    '.cm-cursor, .cm-dropCursor': { borderLeftColor: '#FFFFFF' },
    '.cm-activeLine': { backgroundColor: '#FFBB0044' },
    '.cm-selectionMatch': { backgroundColor: '#006657' },
    '.cm-searchMatch': { backgroundColor: '#006657' },
    '.cm-searchMatch.cm-searchMatch-selected': { backgroundColor: '#006657', outline: '1px solid #FFFFFF' },
} : {
    '&': { backgroundColor: '#FFFFFF', color: '#4A4A4A' },
    '&.cm-editor.cm-focused': { outline: 'none' },
    '&.cm-focused .cm-selectionBackground, .cm-content ::selection': { backgroundColor: '#B3DDFF' },
    '&.cm-focused .cm-matchingBracket, &.cm-focused .cm-nonmatchingBracket': { backgroundColor: '#B86BFFAA' },
    '.cm-gutters': { backgroundColor: '#FFBB00', color: '#0A0A0A', },
    '.cm-activeLineGutter': { backgroundColor: '#242424', color: '#FFFFFF', border: 'none' },
    '.cm-content': { caretColor: '#0A0A0A' },
    '.cm-cursor, .cm-dropCursor': { borderLeftColor: '#0A0A0A' },
    '.cm-activeLine': { backgroundColor: '#FFBB0044' },
    '.cm-selectionMatch': { backgroundColor: '#33FFE0' },
    '.cm-searchMatch': { backgroundColor: '#33FFE0' },
    '.cm-searchMatch.cm-searchMatch-selected': { backgroundColor: '#33FFE0', outline: '1px solid #0A0A0A' },
}, { dark: theme === 'dark' });

const editorHighlight = CodeMirrorLanguage.HighlightStyle.define(theme === 'dark' ? [
    { tag: LezerHighlight.tags.escape, 'background-color': '#F03800' },
    { tag: LezerHighlight.tags.atom, color: '#00CCAD' },
    { tag: LezerHighlight.tags.emphasis, 'font-style': 'italic' },
    { tag: LezerHighlight.tags.strong, 'font-weight': 'bold' },
    { tag: LezerHighlight.tags.strikethrough, 'text-decoration': 'line-through' },
    { tag: LezerHighlight.tags.labelName, 'font-style': 'italic', 'text-decoration': 'underline' },
    { tag: LezerHighlight.tags.contentSeparator, 'font-weight': 'bold' },
    { tag: LezerHighlight.tags.url, color: '#8099FF' },
    { tag: LezerHighlight.tags.link, color: '#8099FF' },
    { tag: LezerHighlight.tags.heading, 'font-weight': 'bold', 'text-decoration': 'underline' },
    { tag: LezerHighlight.tags.quote, 'background-color': '#4A4A4A', color: '#B5B5B5' },
    { tag: LezerHighlight.tags.monospace, color: '#FF0033' },
    { tag: LezerHighlight.tags.special(LezerHighlight.tags.content), 'text-decoration-line': 'underline', 'text-decoration-style': 'dotted' },
] : [
    { tag: LezerHighlight.tags.escape, 'background-color': '#F03800' },
    { tag: LezerHighlight.tags.atom, color: '#009E86' },
    { tag: LezerHighlight.tags.emphasis, 'font-style': 'italic' },
    { tag: LezerHighlight.tags.strong, 'font-weight': 'bold' },
    { tag: LezerHighlight.tags.strikethrough, 'text-decoration': 'line-through' },
    { tag: LezerHighlight.tags.labelName, 'font-style': 'italic', 'text-decoration': 'underline' },
    { tag: LezerHighlight.tags.contentSeparator, 'font-weight': 'bold' },
    { tag: LezerHighlight.tags.url, color: '#4D6DFF' },
    { tag: LezerHighlight.tags.link, color: '#4D6DFF' },
    { tag: LezerHighlight.tags.heading, 'font-weight': 'bold', 'text-decoration': 'underline' },
    { tag: LezerHighlight.tags.quote, 'background-color': '#B5B5B5', color: '#4A4A4A' },
    { tag: LezerHighlight.tags.monospace, color: '#CC0029' },
    { tag: LezerHighlight.tags.special(LezerHighlight.tags.content), 'text-decoration-line': 'underline', 'text-decoration-style': 'dotted' },
]);

const editorExtensions = [
    ...CodeMirror.minimalSetup,
    CodeMirrorView.dropCursor(),
    editorTheme,
    CodeMirrorLanguage.syntaxHighlighting(editorHighlight)
];

if (CodeMirrorL10N[Locale] !== undefined) {
    editorExtensions.push(CodeMirrorState.EditorState.phrases.of(CodeMirrorL10N[Locale]));
}

if (isOptionOn1(StorageSessionEditorSyntaxHighlight, CookieGlobalEditorSyntaxHighlight)) {
    let mdExtensions = [];

    if (isOptionOn1(StorageSessionEditorSyntaxHighlightTable, CookieGlobalEditorSyntaxHighlightTable)) {
        mdExtensions.push(LezerMarkdown.Table);
    }

    if (isOptionOn1(StorageSessionEditorSyntaxHighlightStrikethrough, CookieGlobalEditorSyntaxHighlightStrikethrough)) {
        mdExtensions.push(LezerMarkdown.Strikethrough);
    }

    if (isOptionOn1(StorageSessionEditorSyntaxHighlightSub, CookieGlobalEditorSyntaxHighlightSub)) {
        mdExtensions.push(LezerMarkdown.Subscript);
    }

    if (isOptionOn1(StorageSessionEditorSyntaxHighlightSuper, CookieGlobalEditorSyntaxHighlightSuper)) {
        mdExtensions.push(LezerMarkdown.Superscript);
    }

    if (isOptionOn1(StorageSessionEditorSyntaxHighlightTasklist, CookieGlobalEditorSyntaxHighlightTasklist)) {
        mdExtensions.push(LezerMarkdown.TaskList);
    }

    if (isOptionOn1(StorageSessionEditorSyntaxHighlightEmoji, CookieGlobalEditorSyntaxHighlightEmoji)) {
        mdExtensions.push(LezerMarkdown.Emoji);
    }

    editorExtensions.push(CodeMirrorMarkdown.markdown({ extensions: mdExtensions }));
}

if (isOptionOn1(StorageSessionEditorLineNumbers, CookieGlobalEditorLineNumbers)) {
    editorExtensions.push(CodeMirrorView.lineNumbers());
}

if (isOptionOn1(StorageSessionEditorFolding, CookieGlobalEditorFolding)) {
    editorExtensions.push(CodeMirrorLanguage.foldGutter());
}

if (isOptionOn1(StorageSessionEditorBracketMatching, CookieGlobalEditorBracketMatching)) {
    editorExtensions.push(CodeMirrorLanguage.bracketMatching());
}

if (isOptionOn1(StorageSessionEditorBracketClosing, CookieGlobalEditorBracketClosing)) {
    editorExtensions.push(CodeMirrorAutocomplete.closeBrackets());
}

if (isOptionOn1(StorageSessionEditorRectSelection, CookieGlobalEditorRectSelection)) {
    editorExtensions.push(CodeMirrorView.rectangularSelection());
    editorExtensions.push(CodeMirrorView.crosshairCursor());
}

if (isOptionOn1(StorageSessionEditorActiveLineHighlight, CookieGlobalEditorActiveLineHighlight)) {
    editorExtensions.push(CodeMirrorView.highlightActiveLine());
    editorExtensions.push(CodeMirrorView.highlightActiveLineGutter());
}

if (isOptionOn1(StorageSessionEditorSelectionHighlight, CookieGlobalEditorSelectionHighlight)) {
    editorExtensions.push(CodeMirrorSearch.highlightSelectionMatches());
}

if (isOptionOn1(StorageSessionEditorSearch, CookieGlobalEditorSearch)) {
    editorExtensions.push(CodeMirrorView.keymap.of(CodeMirrorSearch.searchKeymap));
}

const mdParser = getMarkdownParser().use(MarkdownItSourceMap);
const editor = new CodeMirror.EditorView({ extensions: editorExtensions });

function setEditor(title, text) {
    $('#edit-editor-title').text(title === null ? FileTitleDefault : title);
    editor.dispatch({ changes: { from: 0, to: editor.state.doc.length, insert: text === null ? FileContentsDefault : text } });
}

$(document).ready(() => {
    setEditor(sessionStorage.getItem(StorageFileTitle), sessionStorage.getItem(StorageFileContents));
    $('#edit-editor').append(editor.dom);

    $(window).on('beforeunload', () => true);

    $('#edit-save').click(() => {
        sessionStorage.setItem(StorageFileContents, editor.state.doc);
        $(window).off('beforeunload');
    });

    $('#edit-cancel').click((ev) => {
        ev.preventDefault();
        $('#discard-changes-modal').addClass('is-active');
        $('#discard-changes-cancel').focus();
    });

    $('#edit-preview-toggle').click(function () {
        $(this).toggleClass('is-focused');

        let newScrollTop = 0;
        if ($(this).hasClass('is-focused')) {
            updateMarkdownPreview(mdParser, editor.state.doc.toString());

            let editorLineN = 0;
            const editorTopHeightLimit = -editor.documentTop + $('.navbar').outerHeight(true);
            for (let editorTopHeight = 0; editorTopHeight < editorTopHeightLimit && editorLineN < editor.state.doc.lines;) {
                editorTopHeight += editor.lineBlockAt(editor.state.doc.line(++editorLineN).from).height;
            }

            $('#edit-editor').addClass('is-hidden');
            $('#edit-editor-preview').removeClass('is-hidden');

            if (editorLineN) {
                const getPreviewTopHeight = (editorLineN) => {
                    const previewLine = $('#edit-editor-preview > [data-source-line="' + editorLineN.toString() + '"]');
                    return previewLine.length ? previewLine.offset().top : -1;
                };

                newScrollTop = getPreviewTopHeight(editorLineN);
                if (newScrollTop < 0) {
                    let prevPreviewTopHeight = -1;
                    for (let prevEditorLineN = editorLineN - 1; prevEditorLineN > 0 && (prevPreviewTopHeight = getPreviewTopHeight(prevEditorLineN)) < 0; --prevEditorLineN);

                    let nextPreviewTopHeight = -1;
                    for (let nextEditorLineN = editorLineN + 1; nextEditorLineN <= editor.state.doc.lines && (nextPreviewTopHeight = getPreviewTopHeight(nextEditorLineN)) < 0; ++nextEditorLineN);

                    newScrollTop = ((nextPreviewTopHeight < 0 ? $(document).height() : nextPreviewTopHeight) + Math.max(0, prevPreviewTopHeight)) / 2;
                }
            }
        } else {
            let editorLineN = 0;
            const previewTopHeightLimit = $(document).scrollTop() - $('#edit-navbar-block').outerHeight(true) - $('#edit-editor-title').outerHeight(true) - parseInt($('#edit-editor-preview').css('paddingTop'));
            for (let previewTopHeight = 0; previewTopHeight < previewTopHeightLimit && editorLineN < editor.state.doc.lines;) {
                const previewElement = $('#edit-editor-preview > [data-source-line="' + (++editorLineN).toString() + '"]');

                if (previewElement.length) {
                    previewTopHeight += previewElement.outerHeight(true);
                }
            }

            $('#edit-editor-preview').addClass('is-hidden');
            $('#edit-editor').removeClass('is-hidden');

            if (editorLineN) {
                for (let editorLineI = 0; editorLineI < editorLineN;) {
                    newScrollTop += editor.lineBlockAt(editor.state.doc.line(++editorLineI).from).height;
                }

                newScrollTop += $('#edit-navbar-block').outerHeight(true) + $('#edit-editor-title').outerHeight(true) + parseInt($(editor.dom).css('borderTopWidth'));
            }
        }

        $(document).scrollTop(newScrollTop);

        $(this).blur();
    });

    $('#discard-changes-cancel').click(() => {
        $('#discard-changes-modal').removeClass('is-active');
    });

    $('#discard-changes-discard').click(() => {
        $('#discard-changes-modal').removeClass('is-active');
        $(window).off('beforeunload');
        $(location).attr('href', '/');
    });
});
