/*
 * Copyright (C) 2022 Nikola Hadžić
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the ""Software""), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import path from 'path';
import { URL } from 'url';
import fs from 'fs';
import { Command } from 'commander';
import http from 'http';
import https from 'https';
import httpError from 'http-errors';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import express from 'express';
import i18n from 'i18n';

const APP_NAME = 'MaDoIt';
const VIEW_ENGINE = 'pug';

const NODE_DIR = './node_modules';
const PUBLIC_DIR = './public';
const VIEWS_DIR = './views';
const L10N_DIR = './l10n';
const INTROS_DIR = './intros';

/*
 * Turns a raw string into one suitable for String initialization.
 */
String.prototype.raw2JS = function raw2JS(str) {
    if (typeof this !== 'string' && typeof str !== 'string') {
        return null;
    }

    str = typeof this === 'string' ? this : str;

    return str
        .replaceAll('\\', '\\\\')
        .replaceAll("'", "\\'")
        .replaceAll('"', '\\"')
        .replaceAll('\b', '\\b')
        .replaceAll('\t', '\\t')
        .replaceAll('\n', '\\n')
        .replaceAll('\v', '\\v')
        .replaceAll('\f', '\\f')
        .replaceAll('\r', '\\r');
};

const __filename = new URL('', import.meta.url).pathname;
const __dirname = new URL('.', import.meta.url).pathname;

const command = new Command();

command
    .description('Basic HTTP & HTTPS server setup with view engine + cookies + i18n + HTTP errors')
    .usage('[OPTION…]')
    .option('-h, --http-host <address>', 'set the host address', 'localhost')
    .option('-p, --http-port <port>', 'set the HTTP port', 80)
    .option('-s, --https-port <port>', 'set the HTTPS port', 443)
    .option('--no-http', 'do not run HTTP server')
    .option('--no-https', 'do not run HTTPS server')
    .option('-c, --cookies', 'enable usage of cookies')
    .option('--cookies-policy <policy>', 'Same-Site cookies policy', 'Lax')
    .option('--cookies-secure', 'mark cookies with Secure attribute')
    .option('--cookies-always-secure-for-none', 'always mark None cookies with Secure attribute')
    .option('--cookies-expire <days>', 'validity term of the cookies; 0 for session cookies', 365)
    .option('-i, --i18n-methods <method...>', 'i18n methods to use', ['cookie', 'header'])
    .option('--i18n-cookie <name>', 'i18n preference cookie name', 'lang')
    .option('--i18n-header <name>', 'i18n preference header name', 'accept-language')
    .option('--i18n-query <name>', 'i18n preference query parameter name', 'lang')
    .parse(process.argv);

const args = command.opts();

const app = express();

app.set('views', VIEWS_DIR);
app.set('view engine', VIEW_ENGINE);

if (args.cookies) {
    app.use(cookieParser());
}

app.use(bodyParser.json());

const localeFallbacks = {
    'hr':         'sr-Latn',
    'cnr':        'sr-Latn',
    'sr':         'sr-Cyrl',
    'bs':         'sr-Latn',
    'bs-Latn':    'sr-Latn',
    'bs-Cyrl':    'sr-Cyrl',
    'sh':         'sr-Latn'
};

const i18nCookieName = args.i18nMethods.includes('cookie') ? args.i18nCookie : null;

i18n.configure({
    locales:                ['en', 'sr-Cyrl', 'sr-Latn'],
    fallbacks:              localeFallbacks,
    directory:              L10N_DIR,
    retryInDefaultLocale:   true,
    cookie:                 args.cookies ? i18nCookieName : null,
    header:                 args.i18nMethods.includes('header') ? args.i18nHeader : null,   // `null` value results in header name being set to 'accept-language'.
    queryParameter:         args.i18nMethods.includes('query') ? args.i18nQuery : null,
    preserveLegacyCase:     false
});

app.use(i18n.init);

const staticOptions = {
};

app.use(express.static(NODE_DIR, staticOptions));
app.use(express.static(PUBLIC_DIR, staticOptions));

const fileOptions = {
    root:   path.join(__dirname, PUBLIC_DIR)
};

const viewLocals = {
    AppName:                                            APP_NAME,
    MarkdownFlavorDefault:                              'gfm',
    StorageFileTitle:                                   'file_title',
    StorageFileContents:                                'file_contents',
    StorageSessionParserFlavor:                         'session_parser_flavor',
    StorageSessionParserBreak:                          'session_parser_break',
    StorageSessionParserAutolink:                       'session_parser_autolink',
    StorageSessionParserTable:                          'session_parser_table',
    StorageSessionParserStrikethrough:                  'session_parser_strikethrough',
    StorageSessionParserHeaderAnchor:                   'session_parser_sub',
    StorageSessionParserSub:                            'session_parser_sub',
    StorageSessionParserSuper:                          'session_parser_super',
    StorageSessionParserFootnote:                       'session_parser_footnote',
    StorageSessionParserDeflist:                        'session_parser_deflist',
    StorageSessionParserTasklist:                       'session_parser_tasklist',
    StorageSessionParserAbbr:                           'session_parser_abbr',
    StorageSessionParserEmoji:                          'session_parser_emoji',
    StorageSessionParserIns:                            'session_parser_ins',
    StorageSessionParserMark:                           'session_parser_mark',
    StorageSessionEditorSyntaxHighlight:                'session_editor_syntax_highlight',
    StorageSessionEditorSyntaxHighlightTable:           'session_editor_syntax_highlight_table',
    StorageSessionEditorSyntaxHighlightStrikethrough:   'session_editor_syntax_highlight_strikethrough',
    StorageSessionEditorSyntaxHighlightSub:             'session_editor_syntax_highlight_sub',
    StorageSessionEditorSyntaxHighlightSuper:           'session_editor_syntax_highlight_super',
    StorageSessionEditorSyntaxHighlightTasklist:        'session_editor_syntax_highlight_tasklist',
    StorageSessionEditorSyntaxHighlightEmoji:           'session_editor_syntax_highlight_emoji',
    StorageSessionEditorLineNumbers:                    'session_editor_line_numbers',
    StorageSessionEditorFolding:                        'session_editor_folding',
    StorageSessionEditorBracketMatching:                'session_editor_bracket_matching',
    StorageSessionEditorBracketClosing:                 'session_editor_bracket_closing',
    StorageSessionEditorRectSelection:                  'session_editor_rect_selection',
    StorageSessionEditorActiveLineHighlight:            'session_editor_active_line_highlight',
    StorageSessionEditorSelectionHighlight:             'session_editor_selection_highlight',
    StorageSessionEditorSearch:                         'session_editor_search',
};

if (args.cookies) {
    viewLocals.CookiesPolicy = args.cookiesPolicy;
    viewLocals.CookiesSecure = args.cookiesSecure === true;
    viewLocals.CookiesAlwaysSecureForNone = args.cookiesAlwaysSecureForNone === true;
    viewLocals.CookiesExpire = args.cookiesExpire < 1 ? 0 : args.cookiesExpire;
    viewLocals.CookieCookiesAccepted = 'cookies_accepted';
    viewLocals.CookieWarnNotMarkdown = 'warn_not_markdown';
    viewLocals.CookieTheme = 'theme';
    viewLocals.CookieI18N = i18nCookieName;
    viewLocals.CookieGlobalParserFlavor = 'global_parser_flavor';
    viewLocals.CookieGlobalParserBreak = 'global_parser_break';
    viewLocals.CookieGlobalParserAutolink = 'global_parser_autolink';
    viewLocals.CookieGlobalParserTable = 'global_parser_table';
    viewLocals.CookieGlobalParserStrikethrough = 'global_parser_strikethrough';
    viewLocals.CookieGlobalParserHeaderAnchor = 'global_parser_sub';
    viewLocals.CookieGlobalParserSub = 'global_parser_sub';
    viewLocals.CookieGlobalParserSuper = 'global_parser_super';
    viewLocals.CookieGlobalParserFootnote = 'global_parser_footnote';
    viewLocals.CookieGlobalParserDeflist = 'global_parser_deflist';
    viewLocals.CookieGlobalParserTasklist = 'global_parser_tasklist';
    viewLocals.CookieGlobalParserAbbr = 'global_parser_abbr';
    viewLocals.CookieGlobalParserEmoji = 'global_parser_emoji';
    viewLocals.CookieGlobalParserIns = 'global_parser_ins';
    viewLocals.CookieGlobalParserMark = 'global_parser_mark';
    viewLocals.CookieGlobalEditorSyntaxHighlight = 'global_editor_syntax_highlight';
    viewLocals.CookieGlobalEditorSyntaxHighlightTable = 'global_editor_syntax_highlight_table';
    viewLocals.CookieGlobalEditorSyntaxHighlightStrikethrough = 'global_editor_syntax_highlight_strikethrough';
    viewLocals.CookieGlobalEditorSyntaxHighlightSub = 'global_editor_syntax_highlight_sub';
    viewLocals.CookieGlobalEditorSyntaxHighlightSuper = 'global_editor_syntax_highlight_super';
    viewLocals.CookieGlobalEditorSyntaxHighlightTasklist = 'global_editor_syntax_highlight_tasklist';
    viewLocals.CookieGlobalEditorSyntaxHighlightEmoji = 'global_editor_syntax_highlight_emoji';
    viewLocals.CookieGlobalEditorLineNumbers = 'global_editor_line_numbers';
    viewLocals.CookieGlobalEditorFolding = 'global_editor_folding';
    viewLocals.CookieGlobalEditorBracketMatching = 'global_editor_bracket_matching';
    viewLocals.CookieGlobalEditorBracketClosing = 'global_editor_bracket_closing';
    viewLocals.CookieGlobalEditorRectSelection = 'global_editor_rect_selection';
    viewLocals.CookieGlobalEditorActiveLineHighlight = 'global_editor_active_line_highlight';
    viewLocals.CookieGlobalEditorSelectionHighlight = 'global_editor_selection_highlight';
    viewLocals.CookieGlobalEditorSearch = 'global_editor_search';
}

const intros = {
    'en':       { title: 'Introduction.md', contents: String(fs.readFileSync(path.join(__dirname, PUBLIC_DIR, INTROS_DIR, 'Introduction.md'))).raw2JS() },
    'sr-Cyrl':  { title: 'Увод.md', contents: String(fs.readFileSync(path.join(__dirname, PUBLIC_DIR, INTROS_DIR, 'Увод.md'))).raw2JS() },
    'sr-Latn':  { title: 'Uvod.md', contents: String(fs.readFileSync(path.join(__dirname, PUBLIC_DIR, INTROS_DIR, 'Uvod.md'))).raw2JS() },
};

// Set request-dependant view locals.
app.use((req, res, next) => {
    viewLocals.Locale = i18n.getLocale(req);

    viewLocals.FileTitleDefault = intros[viewLocals.Locale].title;
    viewLocals.FileContentsDefault = intros[viewLocals.Locale].contents;

    next();
});

app.get('/', (req, res) => {
    res.render('index', viewLocals);
});

app.get('/edit', (req, res) => {
    res.render('edit', viewLocals);
});

app.get('/settings', (req, res) => {
    res.render('settings', viewLocals);
});

// HTTP error: Not Found (404).
app.use((req, res, next) => {
    next(httpError.NotFound());
});

app.use((err, req, res, next) => {
    console.error('HTTP error: ' + req.ip + ' = ' + err.statusCode + ' (' + err.message + ')');

    switch (err.statusCode) {
        case 404:
            res.redirect(303, '/'); // For 404, we employ redirection to the home page by default.
            break;
        default:
            next()
    }
});

const httpOptions = {
};

if (args.http) {
    http.createServer(httpOptions, app).listen(args.httpPort);
    console.log('Server started at http://' + args.httpHost + ':' + args.httpPort);
}

const httpsOptions = {
    ...httpOptions,
    // TODO: Set website key & certificate.
};

if (args.https) {
    https.createServer(httpsOptions, app).listen(args.httpsPort);
    console.log('Server started at https://' + args.httpHost + ':' + args.httpsPort);
}
